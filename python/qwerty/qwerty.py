#!/usr/bin/env python3

from consolemenu import *
from consolemenu.items import *
import asyncio

menu = ConsoleMenu("Selection", "Because why not")

def printSomething():
    menu.clear_screen()
    print('Something')

def createDefaultMenu():
    function_item = FunctionItem("Print some stuff", printSomething)
    menu.append_item(function_item)
    menu.show()

async def main():
    print('abc')
    createDefaultMenu()
    


asyncio.get_event_loop().run_until_complete(main())