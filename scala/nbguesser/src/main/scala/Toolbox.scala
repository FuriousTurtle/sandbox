package nbguesser

import scala.io.StdIn


object Toolbox {

  def askForInput(): Int = {
    try {
      return StdIn.readInt()
    }
    catch {
      case e: Exception => Console.println(CustomStrings.inputError)
      return askForInput()
    }
  }

  def checkAnswer(userGuess: Int, randNumber: Int): Unit = {
    if (userGuess != randNumber) {
      Console.println(CustomStrings.wrongAnswer)
      Toolbox.checkAnswer(Toolbox.askForInput(), randNumber)
    } else {
      Console.println(CustomStrings.correctAnswer)
    }
  }

}