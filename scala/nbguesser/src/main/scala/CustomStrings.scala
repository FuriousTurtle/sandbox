package nbguesser

object CustomStrings {
    val introduction = "Devinez le chiffre aléatoire entre 1 et 10 :"
    val inputError = "\nMauvais input"
    val wrongAnswer = "\nRaté !"
    val correctAnswer = "\nGagné !"
}