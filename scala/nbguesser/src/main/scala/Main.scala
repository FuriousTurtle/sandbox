package nbguesser

import scala.io.StdIn
import scala.util.Random

object Main extends App {

  val randNumber: Int = Random.nextInt(10)

  Console.println(CustomStrings.introduction)
  Toolbox.checkAnswer(Toolbox.askForInput(), randNumber)

}