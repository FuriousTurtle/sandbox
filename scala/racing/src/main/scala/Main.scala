import racingClasses._

object Main extends App {

    val racingCars = Array(new RacingCar(1), new RacingCar(2), new RacingCar(3))

    def raceEnding(racingCars: Array[RacingCar]) = {
        racingCars.find(RacingCar => RacingCar.currentDistance >= 60) match {
            case Some(car) => 
                println("Player " + car.associatedId + " wins !")
                true
            case None => false
        }
        
    }

    while(!raceEnding(racingCars)){
        VisualOutput.clearScreen()
        for (racingCar <- racingCars) {
            racingCar.updateDistance()
            VisualOutput.printOutput(racingCar)
        }
        Thread.sleep(100)
    }

}