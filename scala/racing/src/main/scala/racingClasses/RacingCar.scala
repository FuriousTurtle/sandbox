package racingClasses

import scala.util.Random

class RacingCar(carId: Int) {
    val associatedId: Int = carId
    
    var currentDistance: Int = 0

    def calcDistance(): Int = {
        currentDistance + Random.nextInt(2)
    }

    def updateDistance(): Unit = {
        this.currentDistance = calcDistance()
    }

}

