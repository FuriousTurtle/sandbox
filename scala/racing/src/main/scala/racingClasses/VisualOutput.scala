package racingClasses

object VisualOutput {

    def printOutput(racingCar: RacingCar): Unit = {
        println("Car " + racingCar.associatedId + " : " + ("=".toString() * racingCar.currentDistance))
    }

    def clearScreen():Unit = {
        print("\u001b[2J")
    }
}
