#pragma once

#include <stdlib.h>
#include <time.h>

using namespace std;

class Randomizer
{

public:
  int generateRandNb()
  {
    srand(time(NULL));
    return rand() % 10 + 1;
  }
};