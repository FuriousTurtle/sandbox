#pragma once
#include <iostream>

using namespace std;

class CustomStrings
{

public:
  string guessTheNumber = "Devinez le chiffre aléatoire entre 1 et 10 : ";
  string wrongInput = "Il faut entrer un chiffre entre 1 et 10 !";
  string biggerThan = "Le chiffre aléatoire est PLUS GRAND que ";
  string smallerThan = "Le chiffre aléatoire est PLUS PETIT que ";
  string goodAnswer = "Gagné ! Le chiffre était : ";
};