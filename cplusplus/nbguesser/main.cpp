#include "Randomizer.cpp"
#include "CustomStrings.cpp"
#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
  Randomizer myRandomizer;
  CustomStrings myStrings;
  
  int randNumber, userAnswer;
  string sAnswer;
  randNumber = myRandomizer.generateRandNb();

  do
  {
    cout << myStrings.guessTheNumber;
    cin >> sAnswer;

    try { userAnswer = stoi(sAnswer); }
    catch(...) {
      cout << myStrings.wrongInput << endl;
      continue;
    }

    string biggerThan = myStrings.biggerThan + to_string(userAnswer);
    string smallerThan = myStrings.smallerThan + to_string(userAnswer);
    if (randNumber != userAnswer) randNumber > userAnswer ? cout << biggerThan << endl : cout << smallerThan << endl;
  } 
  while (randNumber != userAnswer);

  cout << myStrings.goodAnswer + to_string(randNumber) << endl;
}